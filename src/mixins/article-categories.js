export default {
  computed: {
    categories() {
      return [
        {
          name: 'announcements',
          title: 'Анонсы'
        },
        {
          name: 'news',
          title: 'Новости'
        },
        {
          name: 'technologies',
          title: 'Технологии'
        },
        {
          name: 'insides',
          title: 'Инсайды'
        },
        {
          name: 'research',
          title: 'Исследование'
        }
      ]
    }
  },
  methods: {
    getCategory(name) {
      return this.categories.find(item => item.name === name)
    },
    category_href(item) {
      return `/articles/${item.name}`
    }
  }
}
