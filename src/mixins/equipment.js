export default {
  computed: {
    items() {
      return [
        {
          href: "/equipment/water_supply/",
          text: "Водоснабжение",
          name: "water_supply"
        },
        {
          href: "/equipment/heating/",
          text: "Отопление",
          name: "heating"
        },
        {
          href: "/equipment/ventilation/",
          text: "Вентиляция",
          name: "ventilation"
        },
        {
          href: "/equipment/heat_point/",
          text: "Тепловой пункт",
          name: "heat_point"
        },
        {
          href: "/equipment/heating_networks/",
          text: "Тепловые сети",
          name: "heating_networks"
        },
        {
          href: "/equipment/industrial/",
          text: "Промышленное оборудование",
          name: "industrial"
        },
      ]
    }
  }
}
